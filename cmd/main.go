package main

import (
	"github.com/google/uuid"
	"gitlab.com/dpixel/dp-observer/models/changemanager"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/models/observer"
	"gitlab.com/dpixel/dp-observer/models/subject"
)

func main() {
	changemanager, _ := changemanager.New()
	observer_1, _ := observer.New("funny dog", &changemanager)
	observer_2, _ := observer.New("quiet cat", &changemanager)

	subject, _ := subject.New()
	subject.Attach(&observer_1)

	subject.Attach(&observer_2)

	subject.Notify(&event.Event{
		Id:      uuid.New(),
		Message: "Update 1",
	})

	subject.Notify(&event.Event{
		Id:      uuid.New(),
		Message: "Update 2",
	})

	subject.Detach(&observer_2)

	subject.Notify(&event.Event{
		Id:      uuid.New(),
		Message: "Update 3",
	})
}
