SHELL:=/bin/bash

ROOT=${shell pwd}
GO=${shell which go}

EXAMPLE_FILE=${ROOT}/cmd/main.go
TEST_DIR=${ROOT}/tests

.PHONY:
test:
	@mkdir -p ${TEST_DIR}
	@${GO} test ./... -coverprofile ${TEST_DIR}/coverage.out
	@${GO} tool cover -html=${TEST_DIR}/coverage.out

.PHONY:
example-run:
	@${GO} run ${EXAMPLE_FILE}
