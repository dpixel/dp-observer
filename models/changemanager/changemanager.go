package changemanager

import (
	"gitlab.com/dpixel/dp-observer/models/errors"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/models/state"
	repochangemanager "gitlab.com/dpixel/dp-observer/repositories/changemanager"
)

type changeManager struct {
}

func New() (repochangemanager.ChangeManager[event.Event, state.State], error) {
	return &changeManager{}, nil
}

func (changeManager changeManager) Convert(event *event.Event) (*state.State, error) {
	if event == nil {
		return nil, errors.ErrInvalidEvent
	}

	return &state.State{
		Message: event.Message,
	}, nil
}
