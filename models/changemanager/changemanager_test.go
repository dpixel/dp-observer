package changemanager

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dpixel/dp-observer/models/errors"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/models/state"
)

type testSuite struct {
	suite.Suite
	event         event.Event
	state         state.State
	changeManager changeManager
}

func (suite *testSuite) SetupTest() {
	suite.event = event.Event{
		Id:      uuid.New(),
		Message: "NewEvent",
	}

	suite.state = state.State{
		Message: "NewEvent",
	}
}

func (suite *testSuite) TearDownTest() {
}

func TestSuite(t *testing.T) {
	newSuite := new(testSuite)

	suite.Run(t, newSuite)
}

func (suite *testSuite) TestCreatingTheNewObserverIsValid() {
	_, err := New()

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestConvertingTheEventIsValid() {
	state, err := suite.changeManager.Convert(&suite.event)

	assert.Equal(suite.T(), suite.state.Message, state.Message)
	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestConvertingTheIncorrectEventIsInvalid() {
	_, err := suite.changeManager.Convert(nil)

	assert.Equal(suite.T(), errors.ErrInvalidEvent, err)
}
