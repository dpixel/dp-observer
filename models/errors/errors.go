package errors

import "errors"

var (
	ErrInvalidObserver       = errors.New("invalid observer")
	ErrObserverAlreadyExists = errors.New("observer already attached")
	ErrObserverNotFound      = errors.New("observer not found")
	ErrInvalidChangeManager  = errors.New("invalid ChangeManager")
	ErrInvalidEvent          = errors.New("invalid event")
)
