package event

import "github.com/google/uuid"

type Event struct {
	Id      uuid.UUID `json:"id"`
	Message string    `json:"message"`
}
