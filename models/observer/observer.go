package observer

import (
	"fmt"

	"gitlab.com/dpixel/dp-observer/models/errors"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/models/state"
	"gitlab.com/dpixel/dp-observer/repositories/changemanager"
	repoobserver "gitlab.com/dpixel/dp-observer/repositories/observer"
)

type object struct {
	state state.State
}

type observer struct {
	object object
	name   string

	changemanager changemanager.ChangeManager[event.Event, state.State]
}

func New(name string, changemanager *changemanager.ChangeManager[event.Event, state.State]) (repoobserver.Observer[event.Event], error) {
	if changemanager == nil {
		return nil, errors.ErrInvalidChangeManager
	}

	return &observer{
		name:          name,
		changemanager: *changemanager,
	}, nil
}

func (observer *observer) Update(event *event.Event) error {
	if event == nil {
		return errors.ErrInvalidEvent
	}

	state, err := observer.changemanager.Convert(event)
	if err != nil {
		return err
	}
	observer.object.state = *state

	return nil
}

func (observer observer) GetState() {
	fmt.Printf("Observer `%s`, Object State: %s\n", observer.name, observer.object.state.Message)
}
