package observer

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dpixel/dp-observer/mocks"
	"gitlab.com/dpixel/dp-observer/models/changemanager"
	"gitlab.com/dpixel/dp-observer/models/errors"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/models/state"
	repochangemanager "gitlab.com/dpixel/dp-observer/repositories/changemanager"
)

type testSuite struct {
	suite.Suite
	event             event.Event
	state             state.State
	observer          observer
	mockChangeManager *mocks.ChangeManager[event.Event, state.State]
	changemanager     repochangemanager.ChangeManager[event.Event, state.State]
}

func (suite *testSuite) SetupTest() {
	suite.changemanager, _ = changemanager.New()

	suite.event = event.Event{
		Id:      uuid.New(),
		Message: "NewEvent",
	}

	suite.state = state.State{
		Message: "NewEvent",
	}
}

func (suite *testSuite) TearDownTest() {
}

func TestSuite(t *testing.T) {
	newSuite := new(testSuite)

	newSuite.mockChangeManager = new(mocks.ChangeManager[event.Event, state.State])
	newSuite.observer.changemanager = newSuite.mockChangeManager

	suite.Run(t, newSuite)
}

func (suite *testSuite) TestCreatingTheNewObserverIsValid() {
	_, err := New("dummy", &suite.changemanager)

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestCreatingTheNewObserverWithIncorrectChangeManagerIsInvalid() {
	_, err := New("dummy", nil)

	assert.Equal(suite.T(), errors.ErrInvalidChangeManager, err)
}

func (suite *testSuite) TestUpdatingIsValid() {
	suite.mockChangeManager.On("Convert", &suite.event).Return(&suite.state, nil).Once()

	err := suite.observer.Update(&suite.event)

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestUpdatingWithIncorrectEventIsInvalid() {
	err := suite.observer.Update(nil)

	assert.Equal(suite.T(), errors.ErrInvalidEvent, err)
}

func (suite *testSuite) TestUpdatingWithCorrectEventButBrokenStateConvertIsInvalid() {
	suite.mockChangeManager.On("Convert", &suite.event).Return(nil, errors.ErrInvalidEvent).Once()

	err := suite.observer.Update(&suite.event)

	assert.NotEqual(suite.T(), nil, err)
}

func (suite *testSuite) TestGettingStateIsValid() {
	suite.observer.GetState()
}
