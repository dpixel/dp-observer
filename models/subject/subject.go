package subject

import (
	"gitlab.com/dpixel/dp-observer/models/errors"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/repositories/observer"
	reposubject "gitlab.com/dpixel/dp-observer/repositories/subject"
)

type subject struct {
	observers []observer.Observer[event.Event]
}

func New() (reposubject.Subject[event.Event, observer.Observer[event.Event]], error) {
	return &subject{}, nil
}

func (subject *subject) Attach(newObserver *observer.Observer[event.Event]) error {
	if newObserver == nil {
		return errors.ErrInvalidObserver
	}

	for _, observer := range subject.observers {
		if observer == *newObserver {
			return errors.ErrObserverAlreadyExists
		}
	}

	subject.observers = append(subject.observers, *newObserver)
	return nil
}

func (subject *subject) Detach(newObserver *observer.Observer[event.Event]) error {
	if newObserver == nil {
		return errors.ErrInvalidObserver
	}

	for i, observer := range subject.observers {
		if observer == *newObserver {
			subject.observers = append(subject.observers[:i], subject.observers[i+1:]...)
			return nil
		}
	}

	return errors.ErrObserverNotFound
}

func (subject *subject) Notify(event *event.Event) error {
	if event == nil {
		return errors.ErrInvalidEvent
	}

	for _, observer := range subject.observers {
		if err := observer.Update(event); err != nil {
			return err
		}

		observer.GetState()
	}

	return nil
}
