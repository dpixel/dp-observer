package subject

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dpixel/dp-observer/mocks"
	"gitlab.com/dpixel/dp-observer/models/changemanager"
	"gitlab.com/dpixel/dp-observer/models/errors"
	"gitlab.com/dpixel/dp-observer/models/event"
	"gitlab.com/dpixel/dp-observer/models/observer"
	repoobserver "gitlab.com/dpixel/dp-observer/repositories/observer"
)

type testSuite struct {
	suite.Suite
	event               event.Event
	subject             subject
	mockObserver        *mocks.Observer[event.Event]
	observer            repoobserver.Observer[event.Event]
	notAttachedObserver repoobserver.Observer[event.Event]
}

func (suite *testSuite) SetupTest() {
	suite.event = event.Event{
		Id:      uuid.New(),
		Message: "NewEvent",
	}

	changemanager, _ := changemanager.New()
	suite.observer, _ = observer.New("observer", &changemanager)
	suite.subject.Attach(&suite.observer)

	suite.notAttachedObserver, _ = observer.New("not-attached-observer", &changemanager)
}

func (suite *testSuite) TearDownTest() {
}

func TestSuite(t *testing.T) {
	newSuite := new(testSuite)

	newSuite.mockObserver = new(mocks.Observer[event.Event])
	newSuite.subject.observers = append(newSuite.subject.observers, newSuite.mockObserver)

	suite.Run(t, newSuite)
}

func (suite *testSuite) TestCreatingTheNewSubjectIsValid() {
	_, err := New()

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestAttachingTheNewObserverIsValid() {
	err := suite.subject.Attach(&suite.notAttachedObserver)

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestAttachingTheInvalidObserverIsInvalid() {
	err := suite.subject.Attach(nil)

	assert.Equal(suite.T(), errors.ErrInvalidObserver, err)
}

func (suite *testSuite) TestAttachingTheExistingObserverIsInvalid() {
	suite.subject.Attach(&suite.observer)
	err := suite.subject.Attach(&suite.observer)

	assert.Equal(suite.T(), errors.ErrObserverAlreadyExists, err)
}

func (suite *testSuite) TestDetachingTheExistingObserverIsValid() {
	err := suite.subject.Detach(&suite.observer)

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestDetachingTheInvalidObserverIsInvalid() {
	err := suite.subject.Detach(nil)

	assert.Equal(suite.T(), errors.ErrInvalidObserver, err)
}

func (suite *testSuite) TestDetachingTheNonexistingObserverIsInvalid() {
	err := suite.subject.Detach(&suite.notAttachedObserver)

	assert.Equal(suite.T(), errors.ErrObserverNotFound, err)
}

func (suite *testSuite) TestNotifyingWithCorrectEventIsValid() {
	suite.mockObserver.On("Update", mock.Anything).Return(nil).Once()
	suite.mockObserver.On("GetState").Return(nil).Once()

	err := suite.subject.Notify(&suite.event)

	assert.Equal(suite.T(), nil, err)
}

func (suite *testSuite) TestNotifyingWithIncorrectEventIsInvalid() {
	err := suite.subject.Notify(nil)

	assert.Equal(suite.T(), errors.ErrInvalidEvent, err)
}

func (suite *testSuite) TestNotifyingWithCorrectEventButBrokenObserverStateUpdateIsInvalid() {
	suite.mockObserver.On("Update", &suite.event).Return(errors.ErrInvalidEvent).Once()

	err := suite.subject.Notify(&suite.event)

	assert.Equal(suite.T(), errors.ErrInvalidEvent, err)
}
