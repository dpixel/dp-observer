package observer

type Observer[Event any] interface {
	Update(event *Event) error

	// this is a not method of the observer
	GetState()
}
