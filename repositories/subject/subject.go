package subject

type Subject[Event any, Observer any] interface {
	Attach(observer *Observer) error
	Detach(observer *Observer) error
	Notify(event *Event) error
}
